
-- SUMMARY --

URLs should be a) easily readable and b) must be understood by browsers. User
uploaded files can contain special characters that would require escaping,
or they can break your site.

This modules transliterates and cleans the names of new uploaded files by
replacing non-ASCII characters with their unaccented ASCII equivalents and
removes invalid characters [1] altogether. Based on the PHP UTF-8 project [2],
it has support for the full range of Unicode characters (including for example
cyrillic and far eastern character sets).

For a full description visit the project page:
  http://drupal.org/project/file_translit 
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/file_translit/issues

[1] http://www.ietf.org/rfc/rfc2396.txt
[2] http://sourceforge.net/projects/phputf8


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Copy the file_translit module to your modules directory and enable it on the
  Modules page (admin/build/modules).

* That's it. The names of new uploaded files will be transliterated and
  cleaned from invalid characters.


-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - dev@unleashedmind.com
* Stefan M. Kudwien (smk-ka) - dev@unleashedmind.com


This project has been sponsored by:
* UNLEASHED MIND
  Specialized in consulting and planning of Drupal powered sites, UNLEASHED
  MIND offers installation, development, theming, customization, and hosting
  to get you started. Visit http://www.unleashedmind.com for more information.

