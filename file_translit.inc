<?php

/**
 * Sanitize a filename.
 *
 * @param $filename
 *   A filename.
 */
function file_translit_clean($filename) {
  // Trim any leading/trailing dots
  $filename = trim($filename, '.');
  // Transliterate to ASCII
  $filename = file_translit($filename);
  // Replace whitespace
  $filename = str_replace(' ', '_', $filename);
  // Remove any remaining non-safe characters
  $filename = preg_replace('/[^0-9A-Za-z_.-]/', '', $filename);

  return $filename;
}

/**
 * Transliterate UTF-8 text to ASCII.
 * Based on the PHP UTF-8 project http://sourceforge.net/projects/phputf8/
 *
 * @param $string
 *   A UTF-8 encoded string.
 * @param $unknown
 *   Replacement for unknown (erroneous) characters.
 * @return
 *   String transliterated to ASCII.
 */
function file_translit($string, $unknown = '?') {
  // Quickly return if it's pure ASCII
  if(!preg_match('/[\x80-\xff]/', $string)) {
    return $string;
  }

  static $banks = array();
  $result = '';

  for ($i = 0, $len = strlen($string); $i < $len; $i++) {
    $c = $string{$i};
    if (ord($c) >=   0 && ord($c) <= 127) { $result .= $c; continue; } // ASCII - next please
    if (ord($c) >= 192 && ord($c) <= 223) { $ord = (ord($c)-192)*64 + (ord($string{++$i})-128); }
    if (ord($c) >= 224 && ord($c) <= 239) { $ord = (ord($c)-224)*4096 + (ord($string{++$i})-128)*64 + (ord($string{++$i})-128); }
    if (ord($c) >= 240 && ord($c) <= 247) { $ord = (ord($c)-240)*262144 + (ord($string{++$i})-128)*4096 + (ord($string{++$i})-128)*64 + (ord($string{++$i})-128); }
    if (ord($c) >= 248 && ord($c) <= 251) { $ord = (ord($c)-248)*16777216 + (ord($string{++$i})-128)*262144 + (ord($string{++$i})-128)*4096 + (ord($string{++$i})-128)*64 + (ord($string{++$i})-128); }
    if (ord($c) >= 252 && ord($c) <= 253) { $ord = (ord($c)-252)*1073741824 + (ord($string{++$i})-128)*16777216 + (ord($string{++$i})-128)*262144 + (ord($string{++$i})-128)*4096 + (ord($string{++$i})-128)*64 + (ord($string{++$i})-128); }
    if (ord($c) >= 254 && ord($c) <= 255) { $result .= $unknown; continue; } // Error

    $bank = $ord >> 8;
    if (!isset($banks[$bank])) {
      $file = drupal_get_path('module', 'file_translit') .'/translit/'. sprintf('x%02x', $bank) .'.php';
      if (file_exists($file)) {
        $banks[$bank] = include($file);
      } else {
        $banks[$bank] = array();
      }
    }
    
    $char = $ord & 255;
    if (isset($banks[$bank][$char])) {
      $result .= $banks[$bank][$char];
    } else {
      $result .= $unknown;
    }
  }
  
  return $result;
}

